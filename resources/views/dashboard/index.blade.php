<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>Mama and Sons Pizza - Login</title>
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
  </head>
  <body class="layout-default">
    <section class="hero is-fullheight is-medium is-light is-bold">
      <div class="hero-body">
        <div class="container">
          <div class="columns is-centered"">
            <article class="card is-rounded">
              <div class="card-content">
                <h1 class="title has-text-centered">
                  <img src="{{ asset('images/home-icon.png') }}" alt="logo" width="150">
                </h1>
                <div class="field">
                  <p class="control has-icons-left has-icons-right">
                    <input class="input" type="text" placeholder="Username">
                    <span class="icon is-small is-left">
                      <i class="fa fa-user"></i>
                    </span>
                  </p>
                </div>
                <div class="field">
                  <p class="control has-icons-left">
                    <input class="input" type="password" placeholder="Password">
                    <span class="icon is-small is-left">
                      <i class="fa fa-lock"></i>
                    </span>
                  </p>
                </div>
                <div class="field">
                    <p class="control">
                      <button class="button is-success">
                        Login
                      </button>
                    </p>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>
