<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <!-- Styles -->
    <!-- <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"> -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

</head>
<body style="background-color: #f6f6f6;">
    <nav class="navbar is-white" role="navigation">
        <div class="container">
            <div class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item">
                        <img src="{{ asset('images/brand.png') }}" width="150" height ="120" />
                    </a>
                    <a class="navbar-item" href="{{ route('user_dashboard') }}">
                        Profile
                    </a>
                    <a class="navbar-item" href="{{ route('user_orders') }}">
                        Orders
                    </a>

                </div>
                <div class="navbar-end">
                     <a title="View Cart" class="navbar-item" href="{{ route('user_view_cart') }}">
                        <i class="fas fa-shopping-cart"></i>&nbsp;
                    </a>
                    <span class="navbar-item">
                        Hi! {{ Auth::user()->name }}
                    </span>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="navbar-item"><i class="fas fa-user-circle fa-2x"></i>&nbsp;</a>
                        
                    </a>
                </div>
            </div>
        </div>
    </nav>
    @yield('content')
     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
     <script type="text/javascript">
        $(function(){
            $('.delete').click(function() {
                $('.notification').hide();
            });
        });
    </script>
</body>
</html>
