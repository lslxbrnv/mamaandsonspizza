<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <!-- Styles -->
    <!-- <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"> -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

</head>
@php 
     $transaction_count = App\OnlineTransaction::where('status', 1)->count();
@endphp
<body style="background-color: #fff;">
    <nav class="navbar is-dark" role="navigation">
        <div class="container is-fluid">
            <div class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item">
                        <img src="{{ asset('images/brand.png') }}" width="150" height ="120" />
                    </a>
                </div>
                <div class="navbar-end">
                     <a class="navbar-item" href="{{ route('admin_check_orders') }}">
                        @if ($transaction_count > 0)
                            <span class="badge is-badge-success" data-badge="{{ $transaction_count }}"> 
                                Orders 
                            </span>
                        @else
                                Orders
                        @endif
                    </a>
                    <span class="navbar-item">
                        Hi! {{ Auth::user()->name }}
                    </span>
                    <a class="navbar-item" href="{{ route('logout') }}">
                        <i class="fas fa-user-circle fa-2x"></i>&nbsp;
                    </a>
                </div>
            </div>
        </div>
    </nav>
    <div class="container is-fluid">
        <div class="columns is-marginless">
            <div class="column is-3">
                <aside class="menu is-white">
                  <!--   <p class="menu-label">
                      <i class="fas fa-home"></i>  General
                    </p>
                    <ul class="menu-list">
                        <li>
                            <a href="{{ route('admin_dashboard') }}" {{ Request::segment(2) === 'dashboard' ? 'class=is-active' : '' }} id="dashboard">
                                Dashboard
                            </a>
                        </li>
                    </ul> -->
                    <p class="menu-label">
                        <i class="fas fa-utensils"></i> Menu
                    </p>
                    <ul class="menu-list">
                        <li><a href="{{ route('admin_menu') }}" {{ Request::segment(2) === 'menu-list' ? 'class=is-active' : '' }} id="menu-list">Menu List</a></li>
                        <li><a href="{{ route('admin_add_menu_page') }}" {{ Request::segment(2) === 'add-menu' ? 'class=is-active' : '' }} id="add-menu">Add Menu</a></li>
                    </ul>
                    <p class="menu-label">
                        <i class="fas fa-cog"></i> Settings
                    </p>
                    <ul class="menu-list">
                        <li>
                            <a href="{{ route('admin_category') }}" {{ Request::segment(2) === 'categories' ? 'class=is-active' : '' }} id="categories">Categories</a>
                            <ul>
                                <li><a href="{{ route('admin_add_category_page') }}"  {{ Request::segment(2) === 'add-category' ? 'class=is-active' : '' }} id="add-menu-category">Add Menu Category</a></li>
                            </ul>
                        </li>
                        <li>
                        <a href="{{ route('menu_sizes') }}">Sizes</a>
                            <ul>
                                <li><a  href="{{ route('add_menu_sizes') }}" {{ Request::segment(2) === 'sizes' ? 'class=is-active' : '' }} id="add-sizes">Add Sizes</a></li>
                            </ul>
                        </li>
                    </ul>
                    <p class="menu-label">
                        <i class="fas fa-user"></i> Manage Account
                    </p>
                    <ul class="menu-list">
                        <li>
                            <a href="{{ route('admin_create_account') }}" {{ Request::segment(2) === 'create-account' ? 'class=is-active' : '' }} id="create-account">Create Accounts</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="column auto">
                @yield('content')
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>

        </div>
    </div>
    <!-- Scripts -->
    <script type="text/javascript">
        $(function(){
            $('.delete').click(function() {
                $('.notification').hide();
            });
        });
    </script>
 
</body>
</html>
