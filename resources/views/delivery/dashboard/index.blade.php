@extends('layouts.deliveryboy-dashboard')
@section('title', 'Dashboard')
@section('content')
<section class="hero is-warning">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        <i class="fas fa-motorcycle"></i> Deliveries
      </h1>
    </div>
  </div>
</section>
<div class="container">
  <div class="columns is-marginless">
    <div class="column">
      <table class="table">
        <thead>
          <tr>
            <th width="3%"></th>
            <th width="10%" class="has-text-centered">Order No.</th>
            <th width="17%">Name</th>
            <th width="15%">Contact No.</th>
            <th width="55%">Address</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($transactions as $transaction)
          <tr>
            <td></td>
            <td><a href="{{ route('deliveryboy_order', [$transaction->id]) }}">{{ $transaction->transaction_number }}</a></td>
            <td>{{ $transaction->user()->name }}</td>
            <td>{{ $transaction->user()->contact_number }}</td>
            <td>{{ $transaction->user()->address }}</td>
          </tr>
          @empty
          <tr>
            <td colspan="5" class="has-text-centered">No Active Transactions.</td>
          </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
</div>
<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        Mama and Sons &copy; 2018
      </p>
    </div>
  </div>
</footer>
@endsection