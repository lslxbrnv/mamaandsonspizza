@extends('layouts.deliveryboy-dashboard')
@section('title', 'Dashboard')
@section('content')
<section class="hero is-warning">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        <i class="fas fa-motorcycle"></i> Deliveries
      </h1>
    </div>
  </div>
</section>
<div class="container">
  <div class="columns is-marginless is-centered">
    <div class="column is-6">
      <strong>Hi, You have selected the following order:</strong>
    </div>
  </div>
  <div class="columns is-marginless is-centered">
    <div class="column is-6">
      <table class="table is-bordered is-fullwidth">
        <tr>
          <td><strong>Order No.</strong></td><td>{{ $order->transaction_number }}</td>
        </tr>
        <tr>
          <td><strong>Customer Name</strong></td><td>{{ $order->user()->name }}</td>
        </tr>
        <tr>
          <td><strong>Contact No.</strong></td><td>{{ $order->user()->contact_number }}</td>
        </tr>
        <tr>
          <td><strong>Address</strong></td><td>{{ $order->user()->address }}</td>
        </tr>
      </table>
    </div>
  </div>

    <div class="columns is-marginless is-centered">
      <div class="column is-6">
        <p class="has-text-danger has-text-weight-bold">NOTE: Do not close this page once delivery was successful.</p>
      </div>
    </div>
    <div class="columns is-marginless is-centered">
      <div class="column is-6">
        <button class="button is-link">Delivery Sucessful</button>
      </div>
    </div>
</div>
<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        Mama and Sons &copy; 2018
      </p>
    </div>
  </div>
</footer>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ getenv('GOOGLE_API') }}&callback=enableButtons"></script>
<script type="text/javascript">
  (function () {
    if (!window.navigator || !window.navigator.geolocation) {
      alert('Your browser doesn\'t support geolocation!');
      return;
    }
  })

  window.enableButtons = function () {
      var watchId = navigator.geolocation.watchPosition(onLocationChange, onGeolocateError);
      window.localStorage.setItem('lastWatch', watchId);
  }

  function onGeolocateError(error) {
    console.log(error.code, error.message);
    if (error.code === 1) {
      console.log('User declined geolocation');
    } else if (error.code === 2) {
      console.log('Geolocation position unavailable');
    } else if (error.code === 3) {
      console.log('Timeout determining geolocation');
    }
  }

  function onLocationChange(coordinates) {
    var _coordinates$coords2 = coordinates.coords,
      latitude = _coordinates$coords2.latitude,
      longitude = _coordinates$coords2.longitude;
      console.log("longitude:"+longitude+", latitude:"+latitude);
      // ajax here
      $.post("{{ route('deliveryboy_location') }}", { "_token": "{{ csrf_token() }}", "order_id": "{{ $order->id }}", "long": longitude, "lat": latitude });
  }
</script>
@endsection