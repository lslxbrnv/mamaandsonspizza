<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Mama and Sons Pizza</title>
    <!-- Styles -->
    <!-- <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"> -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

</head>

<body class="" style="background-color: #f6f6f6;">
    <nav class="navbar is-primary" role="navigation" aria-label="dropdown navigation">
        <div class="container">
            <div class="navbar-menu">
                <div class="navbar-start">
                    
                </div>
                <div class="navbar-end">
                    <a class="navbar-item">
                        <i class="fas fa-sign-in-alt"></i>&nbsp;Sign In
                    </a>
                </div>
            </div>
        </div>
    </nav>
    <section class="section">
        <div class="container">
            <h1 class="title">
            <img src = "{{ asset('images/home-icon.png') }}" width="200" />
            </h1>
            <p class="subtitle">
            
            </p>
        </div>
    </section>
    <nav class="navbar is-white" role="navigation" aria-label="dropdown navigation">
        <div class="container">
            <div class="navbar-menu">
                <div class="navbar-start">
                    
                </div>
                <div class="navbar-end">
                    <a class="navbar-item">
                        About
                    </a>
                    <a class="navbar-item">
                        Menu
                    </a>
                    <a class="navbar-item">
                        Contact Us
                    </a>
                </div>
            </div>
        </div>
    </nav>
    
        @yield('content')
    
    <!-- Scripts -->
 
</body>
</html>
