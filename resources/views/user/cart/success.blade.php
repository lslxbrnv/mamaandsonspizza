@extends('layouts.user-dashboard')
@section('title', 'Orders')
@section('content')

<!-- <section class="hero is-dark">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        My Orders
      </h1>
    </div>
  </div>
</section> -->

<div class="container is-clearfix">
  <div class="columns is-marginless">
    <div class="column"></div>
    <div class="column is-6">
      <article class="message  is-success">
        <div class="message-header">
          <p> <i class="fas fa-check-circle"></i> Order Sent </p>
        </div>
        <div class="message-body">
          We have received your order. Kindly wait for a call from our representative. Thank you. <br/>
          Order No. <strong><u>{{ str_pad($id, 10, '0', STR_PAD_LEFT) }}</u></strong>
        </div>
      </article>
    </div>
    <div class="column"></div>
  </div>
</div>
<hr>
<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        Mama and Sons &copy; 2018
      </p>
    </div>
  </div>
</footer>
<script type="text/javascript"></script>
@endsection