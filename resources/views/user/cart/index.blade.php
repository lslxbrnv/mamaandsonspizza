@extends('layouts.user-dashboard')
@section('title', 'My Cart')
@section('content')

<section class="hero is-dark">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        My Cart
      </h1>
    </div>
  </div>
</section>

<div class="container is-clearfix">
  <div class="columns">
    <div class="column"></div>
    <div class="column is-9">
      @if(Session::has('message'))
      <div class="notification is-success" id="success-cart">
        <button class="delete"></button>
          Menu has been removed!
      </div>
       @endif
      <form action="{{ route('user_submit_order') }}" method="POST">
        <div class="content">
          <table class="is-bordered is-striped is-narrow is-hoverable">
            <thead>
              <tr>
                <th width="5%"></th>
                <th width="40%"> Menu </th>
                <th width="15%"> Size </th>
                <th width="15%"> Price </th>
                <th width="10%" class="has-text-centered"> Quantity </th>
                <th width="15%" class="has-text-right"> Total </th>
              </tr>
            </thead>
            <tbody>
            @php $total=0; @endphp
            @foreach($carts as $cart)
            <tr>
              <td class="has-text-centered is-size-7">
                <a title="Remove from Cart" href="{{ route('user_remove_cart_item', $cart->id) }}" onclick="return confirm('Are you sure you want to remove this menu?')">
                 <i class="fas fa-minus-circle"></i>
                </a>
              </td>
              <td>{{ $cart->menu->menu }}</td>
              <td>{{ $cart->pricing->sizes->size }}</td>
              <td>{{ $cart->pricing->price }}</td>
              <td class="has-text-centered">
                <input readonly="readonly" class="input quantity" type="text" name="quantity[{{ $cart->id }}]" value="{{ $cart->quantity }}" onkeyup="AdjustTotal(this.value, {{ $cart->id }}, {{ $cart->pricing->price }})">
              </td>
              <td class="has-text-right">
                P <span id="show_price_{{ $cart->id }}">
                    {{ number_format($cart->quantity * $cart->pricing->price, 2) }}
                  </span>
              </td>
            </tr>
            @php $total = $total +( $cart->quantity * $cart->pricing->price) @endphp
            @endforeach
            </tbody>
          </table>
          <div class="content">
            <div class="field">
              <h4 id="total_bill">Total Bill : {{ number_format($total, 2) }} </h4>
            </div>
            <div class="field is-horizontal">
              <div class="field-label is-normal">
                <label class="label">Has change for : </label>
              </div>
              <div class="field-body">
                <div class="field">
                  <p class="control">
                    <input style="width:200px;" class="input" type="" name="has_change_for" placeholder="Change" required>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="control has-text-centered">
          <a href="{{ route('user_menu') }}" class="button">Back to Menu</a>
          <button class="button is-primary">Submit Order</button>
          {!! csrf_field() !!}
        </div>
      </form>
    </div>
    <div class="column"></div>
  </div>
</div>
<hr>
<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        Mama and Sons &copy; 2018
      </p>
    </div>
  </div>
</footer>
<script type="text/javascript">
  $(function(){
    setTimeout(
      function(){
        $('#success-cart').fadeOut(1000);
      }, 3000
    );
  });

   $(function(){
    $(".quantity").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
  });

   function AdjustTotal(total, cartid, price)
   {
      var total_bill = total * price;    
      $('#show_price_'+cartid).html(total_bill.toFixed(2));
   }

</script>
@endsection