@extends('layouts.user-dashboard')
@section('title', 'Dashboard')
@section('content')

<section class="hero is-dark">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Order Menu
      </h1>
    </div>
  </div>
</section>

<div class="container is-clearfix">
  @if(Session::has('message'))
    <div id="success-cart" class="notification is-success">
      Successfully added to Cart!
    </div>
  @endif
  @foreach($menus as $menu)
  <div class="is-pulled-left inline-flex" style="width:285px; padding:5px;">
    <div class="card">
      <div class="card-image">
        <figure class="image is-4by3">
        <?php $location = asset('image/'.$menu->image);  ?>
          <img src="<?php echo $location; ?>" />
        </figure>
      </div>
      <div class="card-content">
        <div class="media">
          <div class="media-content is-clipped" style="height:80px;">
            <p class="title is-6" >{{ $menu->menu }}</p>
            <p class="subtitle is-6">{{ $menu->category->category }}</p>
          </div>
        </div>
        <div class="content" style="overflow-y: scroll; height:30px; max-height:45px;">
          {{ $menu->details }}
        </div>
        <div class="content has-text-centered">
          <button type="button" class="button is-primary is-rounded show-cart" onclick="GetMenuId({{ $menu->id }}, '{{ $menu->menu }}' )">
            <span class="icon"><i class="fas fa-cart-plus"></i></span>
            <span>Add to Cart</span>
          </button>
        </div>
      </div>
    </div>
  </div>   
  @endforeach
</div>
<div class="modal" style="padding:50px;">
  <div class="modal-background"></div>
  <div class="modal-card"  style="width:400px;">
    <form action="{{ route('add_to_cart') }}" method="POST">
    <header class="modal-card-head">
      <p class="modal-card-title has-text-centered" id="modal-menu-name"></p>
      <button type="button" class="delete show-cart" aria-label="close"></button>
    </header>
    <section class="modal-card-body">
      <table class="table is-bordered is-narrow  is-fullwidth menu-table">
        <tbody>
           <td width="40%" class="has-text-centered">Quantity</td>
           <td width="60%" class="has-text-centered">
                <input class="input" type="text" min="1" id="quantity" name="quantity" value="1">
                <input class="input" type="hidden" id="menu_id" name="menu_id" value="">
           </td>
        </tbody>
      </table>
      <div class="columns">
        <div class="column" id="show-details"></div>
        <div class="column">
          <h3>Total</h3>
          <span id="show-total"></span>
        </div>
      </div>
    </section>
    <footer class="modal-card-foot">
      <button type="submit" class="button is-primary">Add to Cart</button>
      <button type="button" class="button show-cart">Cancel</button>
    </footer>
    {{ csrf_field() }}
    </form>
  </div>
</div>
<hr>
<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        Mama and Sons &copy; 2018
      </p>
    </div>
  </div>
</footer>
<script type="text/javascript">
  $(".show-cart").click(function(){
    $('.modal').toggle();
  });
  $(function(){
    setTimeout(
      function(){
        $('#success-cart').fadeOut(1000);
      }, 3000
    );
  });

  $(function(){
    $("#quantity").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
  });

  function GetMenuId(id, menuname)
  {
    $('#modal-menu-name').html(menuname);
    $('#quantity').val(1);
    $('#menu_id').val(id);
    $.ajax({
        type : 'GET',
        url : '{{ route("user_get_menu") }}',
        data : 'menuid='+id,
        cache : false,
        dataType : "html",
        beforeSend: function() {
          $('#show-details').html('<center><img src="{{ asset("images/ajax-loader.gif") }}" /></center>');
        }
      }).done(function(response) {
        $('#show-details').html(response);
        $('input#menu_id').val(id);
        $('input[name="menu_size"]').first().prop('checked', true);
        GetTotal();
        $('input[type=radio][name=menu_size]').change(function() {
          GetTotal();
        });
        $("input[name=quantity]").keyup(function(){
          GetTotal();
        });
      }).fail(function(jqXHR, textStatus, y) {  
        alert(textStatus);
      });
  }

  function GetTotal()
  {
      var qty= $('#quantity').val();
      var pricevar = 'hidden_price'+$('input[name="menu_size"]:checked').val();
      var price = $('input[name="'+pricevar+'"]').val();
      var total = qty * price;
      $('#show-total').html(total.toFixed(2));
  }
</script>
@endsection