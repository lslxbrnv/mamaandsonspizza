@extends('layouts.user-dashboard')
@section('title', 'Orders')
@section('content')

<section class="hero is-dark">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        My Orders
      </h1>
    </div>
  </div>
</section>

<div class="container is-clearfix">
  <div class="columns is-marginless">
    <div class="column"></div>
    <div class="column is-10">
      <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th width="35%" class="has-text-centered">Order No  </th>
            <th width="35%">Transaction Date</th>
            <th width="30%">Status </th>
          </tr>
        </thead>
        <tbody>
          @forelse ($transactions as $transaction)
            <tr>
              <td class="has-text-centered">
                <a href="{{ route('user_order_details', $transaction->id) }}">
                  {{ $transaction->transaction_number }}
                </a>
              </td>
              <td>{{ $transaction->transaction_date }} {{ $transaction->transaction_time }}</td>
              <td>{!! $transaction->current_status !!}</td>
            </tr>
          @empty
            <tr>
              <td colspan="3" class="has-text-centered">No active transaction.</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
    <div class="column"></div>
  </div>
</div>
<hr>
<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        Mama and Sons &copy; 2018
      </p>
    </div>
  </div>
</footer>
<script type="text/javascript"></script>
@endsection