@extends('layouts.user-dashboard')
@section('title', 'Order Details')
@section('content')

<section class="hero is-dark">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Order Details
      </h1>
      <h2 class="subtitle">
        Order No : {{ str_pad($order->id, 10, '0', STR_PAD_LEFT) }}
      </h2>
    </div>
  </div>
</section>

<div class="container is-clearfix">
  <div class="columns is-marginless">
    <div class="column is-4">
      <table class="table is-bordered">
        <tbody>
          <tr>
            <td><strong>Order No.</strong></td>
            <td>{{ $order->transaction_number }}</td>
          </tr>
          <tr>
            <td><strong>Order Details</strong></td>
            <td>
              @foreach($order_menus as $menu)
                <p>{{ $menu->quantity }} x {{ $menu->menu->menu }}</p>
              @endforeach
            </td>
          </tr>
          <tr>
              <td><strong>Total Amount</strong></td>
              <td>{{ number_format($menu->pricing->price * $menu->quantity, 2) }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="column is-8">
      <div id="map" style="width:100%; height: 500px">Loading Map...</div>
    </div>
  </div>
</div>
<hr>
<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        Mama and Sons &copy; 2018
      </p>
    </div>
  </div>
</footer>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ getenv('GOOGLE_API') }}&callback=getPoints"></script>
<script type="text/javascript">
  setInterval(function(){ 
      getPoints();
  }, 5000);

  function getPoints()
  {
    $.get("{{ route('order_location', $order->id) }}",showMap);
  }

  function showMap(response) {
    var $map = document.getElementById('map');
    var position = { lat: parseFloat(response.lat), lng: parseFloat(response.long) };
    window.map = new google.maps.Map($map, {
      center: position,
      zoom: 15
    });

    console.log(map);
    window.markers = window.markers || [];
    var marker = new google.maps.Marker({ map: map, position: position });
  }
</script>
@endsection