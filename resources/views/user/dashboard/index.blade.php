@extends('layouts.user-dashboard')
@section('title', 'Dashboard')
@section('content')
<section class="hero is-dark">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Update Contact Info
      </h1>
    </div>
  </div>
</section>
<div class="container">
  <div class="columns">
    <div class="column"></div>
    <div class="column auto"><br>
      @if ($errors->any())
    <div class="notification is-danger">
      <button class="delete"></button>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
    </div>
    @elseif (Session::has('custom_error'))
    <div class="notification is-danger">
      <button class="delete"></button>
          {{ Session::get('custom_error') }}
      </div>
    @endif
    @if(Session::has('message'))
    <div class="notification is-success">
      <button class="delete"></button>
        Menu has been Added!
    </div>
     @endif
      <form method="POST" action="{{ route('user_update_info') }}">
        <div class="field">
          <label class="label">Name</label>
          <div class="control">
            <input class="input" type="text" placeholder="Name" value="{{ $user->name }}" name="name">
          </div>
        </div>
        <div class="field">
          <label class="label">Current Contact No.</label>
          <div class="control">
            <input class="input" type="text" placeholder="Contact No." value="{{ $user->contact_number }}" name="contact_number">
          </div>
        </div>
        <div class="field">
          <label class="label">Current Address</label>
          <div class="control">
            <input class="input" type="text" placeholder="Address" name="address" value="{{ $user->address }}">
          </div>
        </div>
        <div class="field">
          <label class="label">E-mail</label>
          <div class="control">
            <input class="input" type="text" placeholder="E-mail" name="email" value="{{ $user->email }}">
          </div>
        </div>
        <div class="field">
          <div class="control has-text-right">
            <input type="hidden" placeholder="Address" name="user_id" value="{{ $user->id }}">
            <button class="button is-info">
              <span>Proceed</span>
              <span class="icon is-small">
                <i class="fas fa-caret-right"></i>
              </span>
            </button>
          </div>
        </div>
        {{ csrf_field() }}
      </form>
    </div>
    <div class="column"></div>
  </div>
</div><br>
<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        Mama and Sons &copy; 2018
      </p>
    </div>
  </div>
</footer>
@endsection