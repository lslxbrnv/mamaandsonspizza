<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>Mama and Sons Pizza - Login</title>
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
  </head>
  <body class="layout-default">

    <section class="hero is-fullheight is-light is-bold">

      <div class="hero-body">
        <div class="container">
          @if(Session::has('login-message'))
          <div id="hide-notification" class="columns is-centered">
             <div class="column is-half is-narrow">
              <div class="notification is-danger">
                <button class="delete"></button>
                  <strong>{!! Session::get('login-message') !!}</strong>    
                <!-- @if(Session::has('reset-success-message'))
                  <div id="message">
                  <div align="center" class="small-12 large-12 columns success callout">
                  <strong>{!! Session::get('reset-success-message') !!}</strong>
                  </div>
                  </div>
                @endif -->
              </div>
            </div>
          </div>
           @endif

          <div class="columns is-centered">
            <article class="card is-rounded">
              <div class="card-content">
                <h1 class="title has-text-centered">
                  <img src="{{ asset('images/home-icon.png') }}" alt="logo" width="150">
                </h1>
                <form method="POST" action="{{ route('verify_login') }}">
                  <div class="field">
                    <p class="control has-icons-left has-icons-right">
                      <input class="input" type="text" placeholder="Username" name="username" required>
                      <span class="icon is-small is-left">
                        <i class="fa fa-user"></i>
                      </span>
                    </p>
                  </div>
                  <div class="field">
                    <p class="control has-icons-left">
                      <input class="input" type="password" placeholder="Password" name="password" required>
                      <span class="icon is-small is-left">
                        <i class="fa fa-lock"></i>
                      </span>
                    </p>
                  </div>
                  <div class="field">
                      <p class="control">
                        <button class="button is-success">
                          Register
                        </button>
                         <button class="button is-primary">
                          Login
                        </button>
                      </p>
                  </div>
                  {{ csrf_field() }}
                </form>
                 <div class="field">
                    <p class="control">
                      <small class="has-text-weight-light is-size-7">Forgot Password?</small>
                    </p>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </section>
    <script type="text/javascript">
      $(function(){
        $('.delete').click(function() {
          $('#hide-notification').hide();
        });
      });
    </script>
  </body>
</html>
