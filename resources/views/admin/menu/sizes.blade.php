@extends('layouts.admin-dashboard')

@section('title', 'Menu - Manage Sizes')
@section('content')
	
<br />

<div class="columns">
	<div class="column is-9">
		<h1 class="title">Manage Menu</h1>
		<h3 class="subtitle">{{ $menu->menu }}</h3>
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				Menu has been updated.
		</div>
		 @endif
		 @if ($errors->any())
		<div class="notification is-danger">
			<button class="delete"></button>
	        @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
		</div>
		@elseif (Session::has('custom_error'))
		<div class="notification is-danger">
			<button class="delete"></button>
	        {{ Session::get('custom_error') }}
	    </div>
		@endif
		<form action="{{ route('admin_add_price_size') }}" method="POST">
			<div class="field is-grouped">
				<div class="select">
					<select name="size">
						<option></option>
						@foreach($sizes as $size)
						<option title="{{ $size->size }}" value="{{ $size->id }}">{{ $size->size }}</option>
						@endforeach
					</select>
				</div>
				<input class="input" type="text" placeholder="Price" id="price" name="price">
				<input type="hidden"  name="menu_id" value="{{ $id }}">
				<button type="submit" class="button is-info">Add</button>
			</div>
			{!! csrf_field() !!}

		</form>
		<br>
		<table id="category-list" class="table is-hovered is-bordered is-fullwidth">
			<thead>
				<tr>
					<th width="10%" class="has-text-centered">Action</th>
					<th width="60%">Size</th>
					<th width="30%" class="has-text-right">Price</th>
				</tr>
			</thead>
			<tbody>
				@forelse($pricing as $price)
				<tr>
					<td class="has-text-centered">
						<a onclick="return confirm('Are you sure you want to remove size price?')" href="{{ route('admin_remove_menu_price', [$price->id, $price->menu_id]) }}" title="Remove Size Price">
							<span class="icon has-text-info"><i class="far fa-minus-square"></i></span>
						</a>
					</td>
					<td> {{ $price->sizes->size }}</td>
					<td class="has-text-right">{{ $price->price }}</td>
				</tr>
				@empty
				<tr>
					<td colspan="3" class="has-text-centered">No Price added.</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$("#price").keyup(function(event) {				
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
			(event.keyCode == 65 && event.ctrlKey === true) || 
			(event.keyCode >= 35 && event.keyCode <= 39)) {
			return;
		}
		else {
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
			event.preventDefault(); 
			}   
		}
	});
</script>
@endsection