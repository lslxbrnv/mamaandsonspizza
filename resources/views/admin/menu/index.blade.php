@extends('layouts.admin-dashboard')

@section('title', 'Menu')
@section('content')

<br />

<div class="columns">
	<div class="column is-9">
		<h1 class="title">Menu</h1>
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				Menu has been deleted!
		</div>
		 @endif
		<table id="category-list" class="table is-hovered is-bordered is-fullwidth">
			<thead>
				<tr>
					<th width="20%" class="has-text-centered">Action</th>
					<th width="35%">Category</th>
					<th width="45%">Menu</th>
				</tr>
			</thead>
			<tbody>
				@forelse($menus as $menu)
				<tr id="menu-list-{{ $menu->id }}">
					<td class="has-text-centered">
						<a onclick="return confirm('Are you sure you want to remove this menu?')" title="Remove Menu" href="{{ route('admin_remove_menu', $menu->id) }}">
							<span class="icon has-text-info"><i class="far fa-minus-square"></i></span>
						</a>
						<a href="{{ route('admin_edit_menu_page', $menu->id) }}" title="Edit Menu">
							<span class="icon has-text-info"><i class="fas fa-edit"></i></span>
						</a>
						<a href="{{ route('admin_manage_sizes',  $menu->id)  }}" title="Manage Sizes">
							<span class="icon has-text-info"><i class="fas fa-adjust"></i></span>
						</a>
					</td>
					<td>{{ $menu->category->category }}</td>
					<td>{{ $menu->menu }}</td>
				</tr>
				@empty
				<tr>
					<td colspan="3" class="has-text-centered">No Menu added yet.</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>

@endsection