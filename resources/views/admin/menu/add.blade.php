@extends('layouts.admin-dashboard')

@section('title', 'Menu - Add')
@section('content')
	
<br />
<div class="columns">
	<div class="column is-6">
		<h1 class="title">Add Menu</h1>
		@if ($errors->any())
		<div class="notification is-danger">
			<button class="delete"></button>
	        @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
		</div>
		@elseif (Session::has('custom_error'))
		<div class="notification is-danger">
			<button class="delete"></button>
	        {{ Session::get('custom_error') }}
	    </div>
		@endif
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				Menu has been Added!
		</div>
		 @endif
		{!! Form::open(array('route' => 'admin_verify_menu','files'=>true)) !!}
			<div class="field">
				<label class="label">Category</label>
				<div class="control">
					<div class="select">
						<select name="category">
							<option></option>
							@foreach($categories as $category)
							<option title="{{ $category->category }}" value="{{ $category->id }}">{{ $category->category }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="field">
				<label class="label">Menu</label>
				<div class="control">
					<input class="input" type="text" placeholder="Menu" name="menu" value="{{ old('menu') }}">
				</div>
			</div>

			<div class="field">
				<label class="label">Details</label>
				<div class="control">
					<textarea name="details" class="textarea" type="text" placeholder="Details">{{ old('details') }}</textarea>
				</div>
			</div>

			<div class="file">
				<label class="file-label">
					 {!! Form::file('image', array('class' => 'file-input')) !!}
					<span class="file-cta">
						<span class="file-icon">
							<i class="fas fa-upload"></i>
						</span>
						<span class="file-label">
							Choose a file…
						</span>
					</span>
				</label>
			</div>

			<div class="field">
				<div class="control is-pulled-right">
				  <button class="button is-info">Add</button>
				</div>
				<div class="control is-pulled-right">
				  <a class="button is-light" href="{{ route('admin_menu') }}"> Back</a>&nbsp;
				</div>
			</div>
			{!! csrf_field() !!}
		{!! Form::close() !!}
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('select[name=category]').focus();
	});
</script>
@endsection