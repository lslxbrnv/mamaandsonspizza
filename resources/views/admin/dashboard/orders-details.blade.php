@extends('layouts.admin-dashboard')

@section('title', 'Order Details')
@section('content')
<script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.19.0.min.js"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
	/* Always set the map height explicitly to define the size of the div * element that contains the map. */ 
	#map { height: 100%; } /* Optional: Makes the sample page fill the window. */ html, 
	body { height: 100%; margin: 0; padding: 0; }

</style>
<div class="columns is-marginless">
	<div class="column is-9">
		<h1 class="title">Order Details</h1>
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				Menu has been deleted!
		</div>
		 @endif
		 <div class="content">
		 	<p> <strong>Name : </strong> {{ $transaction->user->name }} </p>
		 	<p> <strong>Contact Number : </strong> {{ $transaction->user->contact_number }} </p>
		 	<p> <strong> Address :  </strong>{{ $transaction->user->address }} </p>
		 	<p> <strong> E-mail : </strong>{{ $transaction->user->email }} </p>

		 	<h3> Orders :  </h3>
		 	<table class="table">
		 		<thead>
		 			<tr>
		 				<th>Menu</th>
		 				<th>Quantity</th>
		 				<th>Price</th>
		 				<th>Total</th>
		 			</tr>
		 		</thead>
		 		<tbody>
		 			@php $total_bill = 0; @endphp
				 	@foreach ($orders as $order)
				 	<tr>
				 		<td>
				 			{{ $order->menu->menu }}
				 		</td>
				 		<td>
				 			{{ $order->quantity }}
				 		</td>
				 		<td>
				 			P {{ $order->pricing->price }}
				 		</td>
				 		<td>
				 			P {{ number_format($order->quantity * $order->pricing->price,2) }}
				 			@php $total_bill = $total_bill + ($order->quantity * $order->pricing->price) @endphp
				 		</td>
				 	</tr>
				 	@endforeach	
			 	</tbody>
		 	</table>
		 	<hr>
		 	<p><strong>Total Bill : </strong> P {{ number_format($total_bill,2) }} </p>
		 	<p><strong>Change for : </strong> P {{ $transaction->has_change }} </p>
		 </div>
		 <p>
		 	
		 </p>
	</div>
</div>
  <div id="map-canvas" style="width:600px;height:400px"></div>
<!-- Replace the value of the key parameter with your own API key. --> 
<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjNjNbjTmb_ri6vOjOWruDkr0SQPwQI1s&callback=initMap"> </script> -->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAjNjNbjTmb_ri6vOjOWruDkr0SQPwQI1s&callback=initialize"></script>
<script type="text/javascript">
	var pubnub = new PubNub({
      publishKey:   'pub-c-cc20fa54-a88c-4c37-8d97-3df7cf81e23b',
      subscribeKey: 'YOUR_SUB_KEY'
    });

     var map;
    var mark;
    var initialize = function() {
      map  = new google.maps.Map(document.getElementById('map-canvas'), {center:{lat:lat,lng:lng},zoom:12});
      mark = new google.maps.Marker({position:{lat:lat, lng:lng}, map:map});
    };
    window.initialize = initialize;
</script>
@endsection