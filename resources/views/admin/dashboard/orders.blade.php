@extends('layouts.admin-dashboard')

@section('title', 'Orders')
@section('content')

<div class="columns">
	<div class="column is-9">
		<h1 class="title">Orders</h1>
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				{{ Session::get('message') }}
		</div>
		 @endif
		<table id="order-list" class="table is-hovered is-bordered is-fullwidth">
			<thead>
				<tr>
					<th width="15%" class="has-text-centered">Action</th>
					<th width="50%">Order No. </th>
					<th width="35%">Current Status</th>
				</tr>
			</thead>
			<tbody>
				@forelse ($transactions as $transaction)
				<tr>
					<td class="has-text-centered ">
						<a href="{{ route('admin_check_order_details', $transaction->id) }}" title="Order Details">
							<span class="icon has-text-info is-clearfix"><i class="fas fa-info"></i></span>
						</a>
						{!! $transaction->change_status !!}
					</td>
					<td>{{ $transaction->transaction_number }}</td>
					<td>{!! $transaction->current_status !!}</td>
				</tr>
				@empty
				<tr>
					<td colspan="3" class="has-text-centered">No active transactions.</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>


@endsection