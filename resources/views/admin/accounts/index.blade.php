@extends('layouts.admin-dashboard')

@section('title', 'Create Account')
@section('content')

<div class="columns is-marginless">
	<div class="column is-8">
		<h1 class="title">Create Account</h1>
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				{{ Session::get('message') }}
		</div>
		 @endif
		<form method="POST" action="{{ route('admin_add_account') }}">
			<div class="field">
				<label class="label">Name</label>
				<div class="control">
					<input class="input" type="text" placeholder="Name" name="name" value="{{ old('name') }}">
				</div>
			</div>
			<div class="field">
				<label class="label">Username</label>
				<div class="control">
					<input class="input" type="text" placeholder="Username" name="username" value="{{ old('username') }}">
				</div>
			</div>
			<div class="field">
				<label class="label">Email</label>
				<div class="control">
					<input class="input" type="email" placeholder="Email" name="email" value="{{ old('email') }}">
				</div>
			</div>
			<div class="field">
				<label class="label">Password</label>
				<div class="control">
					<input class="input" type="password" placeholder="Password" name="password">
				</div>
			</div>
			<div class="field">
				<div class="control is-pulled-right">
				  <button type="submit" class="button is-info">Create</button>
				</div>
			</div>
			{!! csrf_field() !!}
		</form>
	</div>
</div>
<script type="text/javascript">
	
</script>
@endsection