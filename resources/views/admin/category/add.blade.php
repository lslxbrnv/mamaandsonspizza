@extends('layouts.admin-dashboard')

@section('title', 'Category - Add')
@section('content')
	
<br />
<div class="columns">
	<div class="column is-6">
		<h1 class="title">Add Category</h1>
		@if ($errors->any())
		<div class="notification is-danger">
			<button class="delete"></button>
	        @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
		</div>
		@elseif (Session::has('custom_error'))
		<div class="notification is-danger">
			<button class="delete"></button>
	        {{ Session::get('custom_error') }}
	    </div>
		@endif
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				Menu Category has been Added!
		</div>
		 @endif
		<form method="POST" action="{{ route('admin_verify_category') }}">
			<div class="field">
				<label class="label">Category</label>
				<div class="control">
					<input class="input" type="text" placeholder="Category Name" name="category" autocomplete="off">
				</div>
			</div>
			<div class="field">
				<div class="control is-pulled-right">
				  <button class="button is-info">Add</button>
				</div>
				<div class="control is-pulled-right">
				  <a class="button is-light" href="{{ route('admin_category') }}"> Back</a>&nbsp;
				</div>
			</div>
			{!! csrf_field() !!}
		</form>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('input[name=category]').focus();
	});
</script>
@endsection