@extends('layouts.admin-dashboard')

@section('title', 'Menu Category')
@section('content')

<br />
<div class="columns">
	<div class="column is-8">
		<h1 class="title">Menu Category</h1>
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				Menu Category has been deleted!
		</div>
		 @endif
		<table id="category-list" class="table is-hovered is-bordered is-fullwidth">
			<thead>
				<tr>
					<th width="15%" class="has-text-centered">Action</th>
					<th width="85%">Category</th>
				</tr>
			</thead>
			<tbody>
				@forelse($categories as $category)
				<tr id="category-list-{{ $category->id }}">
					<td class="has-text-centered">
						<a onclick="return confirm('Removing this category will also remove menus under it. Are you sure you want to continue?')"  href="{{ route('admin_remove_category', $category->id) }}" title="Remove Category">
							<span class="icon has-text-info"><i class="far fa-minus-square"></i></span>
						</a>
						<a href="{{ route('admin_edit_category_page', $category->id) }}" title="Edit Category">
							<span class="icon has-text-info"><i class="fas fa-edit"></i></span>
						</a>
					</td>
					<td>{{ $category->category }}</td>
				</tr>
				@empty
				<tr>
					<td colspan="2" class="has-text-centered">No Categories added yet.</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	
</script>
@endsection