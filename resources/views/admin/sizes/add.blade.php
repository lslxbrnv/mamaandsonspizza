@extends('layouts.admin-dashboard')

@section('title', 'Sizes - Add')
@section('content')
	
<br />
<div class="columns">
	<div class="column is-6">
		<h1 class="title">Add Sizes</h1>
		@if ($errors->any())
		<div class="notification is-danger">
			<button class="delete"></button>
	        @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
		</div>
		@elseif (Session::has('custom_error'))
		<div class="notification is-danger">
			<button class="delete"></button>
	        {{ Session::get('custom_error') }}
	    </div>
		@endif
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				Size has been Added!
		</div>
		 @endif
		<form method="POST" action="{{ route('admin_verify_added_size') }}">
			<div class="field">
				<label class="label">Size</label>
				<div class="control">
					<input value="{{ old('size') }}" class="input" type="text" placeholder="Size" name="size" autocomplete="off">
				</div>
			</div>
			<div class="field">
				<div class="control is-pulled-right">
				  <button class="button is-info">Add</button>
				</div>
				<div class="control is-pulled-right">
				  <a class="button is-light" href="{{ route('menu_sizes') }}"> Back</a>&nbsp;
				</div>
			</div>
			{!! csrf_field() !!}
		</form>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('input[name=size]').focus();
	});
</script>
@endsection