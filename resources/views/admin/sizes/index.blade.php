@extends('layouts.admin-dashboard')

@section('title', 'Sizes')
@section('content')

<br />
<div class="columns">
	<div class="column is-8">
		<h1 class="title">Sizes</h1>
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				Size has been deleted!
		</div>
		 @endif
		<table id="category-list" class="table is-hovered is-bordered is-fullwidth">
			<thead>
				<tr>
					<th width="10%" class="has-text-centered">Action</th>
					<th width="90%">Size</th>
				</tr>
			</thead>
			<tbody>
				@forelse($sizes as $size)
				<tr>
					<td class="has-text-centered">
						<a onclick="return confirm('Are you sure you want to remove size?')" title="Remove Size" href="{{ route('admin_remove_size', $size->id) }}">
							<span class="icon has-text-info"><i class="far fa-minus-square"></i></span>
						</a>
					</td>
					<td>{{ $size->size }}</td>
				</tr>
				@empty
				<tr>
					<td colspan="2" class="has-text-centered">No added sizes yet.</td>
				</tr>	
				@endforelse
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	
</script>
@endsection