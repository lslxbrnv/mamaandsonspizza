<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('auth/{provider}', ['as' => 'socialite_login', 'uses' => 'Auth\LoginController@redirectToProvider']);
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


Route::group(['middleware' => 'auth'], function(){
	Route::get('/', 'SiteController@index')->name('site-home');
	Route::get('/admin', 'AdminController@index')->name('admin-login');
	#admin
	Route::get('admin/dashboard', 'DashboardController@index')->name('admin_dashboard');
	Route::get('admin/orders', 'DashboardController@check_orders')->name('admin_check_orders');
	Route::get('admin/order-details/{id}', 'DashboardController@check_order_details')->name('admin_check_order_details');

	Route::get('admin/receive-order/{id}', 'DashboardController@receive_order')->name('admin_receive_order');
	Route::get('admin/set-delivery-boy/{id}', 'DashboardController@set_delivery_boy')->name('admin_set_delivery_boy');

	Route::get('admin/create-account', 'DashboardController@create_account')->name('admin_create_account');
	Route::post('admin/add-account', 'DashboardController@add_account')->name('admin_add_account');

	#category
	Route::get('admin/categories', 'CategoryController@index')->name('admin_category');
	Route::get('admin/add-category', 'CategoryController@add_category_page')->name('admin_add_category_page');

	Route::get('admin/edit-category/{id}', 'CategoryController@edit_category_page')->name('admin_edit_category_page');
	Route::get('admin/remove-category/{id}', 'CategoryController@remove_category')->name('admin_remove_category');

	Route::post('admin/verify-category', 'CategoryController@verify_category')->name('admin_verify_category');

	#menu 
	Route::get('admin/menu-list', 'MenuController@index')->name('admin_menu');
	Route::get('admin/add-menu', 'MenuController@add_menu_page')->name('admin_add_menu_page');
	Route::get('admin/edit-menu', 'MenuController@edit_menu_page')->name('admin_edit_menu_page');
	Route::post('admin/verify-menu', 'MenuController@verify_menu')->name('admin_verify_menu');

	Route::get('admin/edit-menu/{id}', 'MenuController@edit_menu_page')->name('admin_edit_menu_page');
	Route::get('admin/remove-menu/{id}', 'MenuController@remove_menu')->name('admin_remove_menu');

	#menu-price
	Route::get('admin/manage-sizes/{id}', 'MenuController@manage_sizes')->name('admin_manage_sizes');
	Route::post('admin/add-price-size/', 'MenuController@add_price_size')->name('admin_add_price_size');
	Route::get('admin/remove-menu-price/{id}/{pid}', 'MenuController@remove_menu_price')->name('admin_remove_menu_price');

	#sizes
	Route::get('admin/menu-sizes', 'SizesController@index')->name('menu_sizes');
	Route::get('admin/add-menu-sizes', 'SizesController@add_menu_sizes')->name('add_menu_sizes');
	Route::post('admin/verify-added-size', 'SizesController@verify_added_size')->name('admin_verify_added_size');

	Route::get('admin/remove-size/{id}', 'SizesController@remove_size')->name('admin_remove_size');

	################## USER #############################
	Route::get('user/dashboard', 'UserDashboardController@index')->name('user_dashboard');
	Route::post('user/update-info', 'UserDashboardController@update_info')->name('user_update_info');

	Route::get('user/get-menu', 'UserMenuController@get_menu')->name('user_get_menu');
	Route::post('user/add-to-cart', 'UserMenuController@add_to_cart')->name('add_to_cart');

	Route::get('user/view-cart', 'CartController@view_cart')->name('user_view_cart');
	Route::get('user/remove-cart-item/{id}', 'CartController@user_remove_cart_item')->name('user_remove_cart_item');

	Route::post('user/submit-order', 'CartController@submit_order')->name('user_submit_order');
	Route::get('user/success-order/{id}', 'CartController@success_order')->name('user_success_order');

	Route::get('user/menu', 'UserMenuController@index')->name('user_menu');
	Route::get('user/orders', 'UserMenuController@user_orders')->name('user_orders');
	Route::get('user/order-details/{id}', 'UserMenuController@user_order_details')->name('user_order_details');
	Route::get('user/order-location/{id}', 'UserMenuController@user_order_location')->name('order_location');

	######## Delivery Boy #################
	Route::get('delivery/dashboard', 'DeliveryController@index')->name('deliveryboy_dashboard');	
	Route::get('delivery/{id}', 'DeliveryController@selectOrder')->name('deliveryboy_order');
	Route::post('delivery/location', 'DeliveryController@saveLocation')->name('deliveryboy_location');
});