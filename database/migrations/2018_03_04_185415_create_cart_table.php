<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration
{
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->integer('price_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('quantity');


            $table->foreign('menu_id')->references('id')->on('menu');
            $table->foreign('price_id')->references('id')->on('pricing');
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->timestamps();

            $table->index(['menu_id', 'price_id', 'user_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
