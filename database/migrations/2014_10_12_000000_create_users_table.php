<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 150)->unique();
            $table->string('email', 150)->unique();
            $table->string('password', 80)->nullable();
            $table->string('name', 200)->nullable();
            $table->string('contact_number', 50)->nullable();
            $table->string('address', 255)->nullable();
            $table->tinyInteger('access')->nullable();
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
   

        App\User::create([
            'username' => 'admin',
            'email' => 'nathanzaragoza1@gmail.com',
            'password' => bcrypt('p@ssw0rd'),
            'name' => 'Administrator',
            'access' => 1
        ]);

        App\User::create([
            'username' => 'customer',
            'email' => 'nathanzaragoza2@gmail.com',
            'password' => bcrypt('p@ssw0rd'),
            'name' => 'User',
            'access' => 2
        ]);

         App\User::create([
            'username' => 'deliveryboy',
            'email' => 'nathanzaragoza3@gmail.com',
            'password' => bcrypt('p@ssw0rd'),
            'name' => 'User',
            'access' => 3
        ]);
     }
     
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
