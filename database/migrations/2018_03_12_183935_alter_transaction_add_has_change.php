<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionAddHasChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('online_transaction'))
        {
            Schema::table('online_transaction', function(Blueprint $table){
                $table->decimal('has_change', 10,2);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('online_transaction', function ($table) {
            $table->dropColumn(['has_change']);
        });
    }
}
