<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    public $table = 'users';
    public $timestamps = true;

    protected $fillable = [
        'email', 'name', 'contact_number', 'address', 'username', 'password', 'access', 'remember_token', 'created_at', 'updated_at', 'deleted_at', 'provider', 'provider_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cart()
    {
        return $this->hasMany('App\Cart');
    
    }

    public function transaction()
    {
        return $this->hasMany('App\Transaction');
    
    }

}
