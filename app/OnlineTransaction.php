<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineTransaction extends Model
{
    protected $connection = 'mysql2';

    public $table = 'online_transaction';
    public $fillable = ['transaction_date', 'transaction_time', 'user_id', 'created_at', 'has_change'];  
    public $timestamps = false;
    
    public function user()
    {
        return \App\User::find($this->user_id);
    }

    public function online_transaction_menu()
    {
        return $this->belongsTo('App\OnlineTransactionMenu', 'id', 'transaction_id');
    }

    public function menus()
    {
        return $this->hasMany('App\OnlineTransactionMenu');
    }

    public function getTransactionNumberAttribute()
    {

        return str_pad($this->id, 10, '0', STR_PAD_LEFT);
    }

    public function getCurrentStatusAttribute()
    {
    	switch($this->status) 
    	{
    		case 1:
                return '<i class=" fas fa-spinner fa-pulse  has-text-warning"></i> Pending';
            break;
            case 2:
                return '<i class="fas fa-check-circle has-text-success"></i> Received';
            break;
            case 3:
                return 'Preparing Order';
            break;
    	}
    }
    public function getChangeStatusAttribute()
    {
        switch($this->status)
        {
            case 1:
                return '<a href="'. route('admin_receive_order', $this->id) .'" title="Receive Order">
                            <span class="icon has-text-success is-clearfix"><i class="fas fa-check "></i></span>
                        </a>';
            break;

            case 2:
              return '<a href="#" title="Pending for delivery"><span class="icon has-text-dark is-clearfix"><i class="fas fa-stopwatch"></i></span></a>';
            break;

        }
    }

    
}
