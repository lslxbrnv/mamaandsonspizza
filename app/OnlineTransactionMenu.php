<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineTransactionMenu extends Model
{
    protected $connection = 'mysql2';
    public $table = 'online_transaction_menu';
    public $fillable = ['transaction_id', 'menu_id', 'quantity', 'price_id'];  
    public $timestamps = false;
    
    public function online_transaction()
    {
        return $this->belongsTo('App\OnlineTransaction', 'transaction_id', 'id');
    }

    public function menu()
    {
        return $this->belongsTo('App\Menu', 'menu_id', 'id');
    }

    public function pricing()
    {
        return $this->belongsTo('App\Pricing', 'price_id', 'id');
    }
}
