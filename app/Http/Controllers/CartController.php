<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Response;
use \Carbon\Carbon;

use App\Category;
use App\Menu;
use App\Pricing;
use App\Cart;
use App\Sizes;
use App\OnlineTransaction;
use App\OnlineTransactionMenu;

class CartController extends Controller
{
    public function view_cart()
    {
    	$carts = Cart::with('menu')
    				 ->with('pricing')
    				 ->where('user_id', Auth::user()->id)
    				 ->orderBy('menu_id', 'ASC')
    				 ->get();


    	$data = compact('carts');

    	return view('user.cart.index', $data);
    }

    public function user_remove_cart_item($menuid)
    {
    	$cart = Cart::find($menuid);
        $cart->delete();

        return redirect()->route('user_view_cart')->with('message', 'Success');
    }

    public function submit_order(Request $request)
    {

    	#dd($request->quantity);

 		$transaction = new OnlineTransaction;
        $transaction->transaction_date = Carbon::now();
        $transaction->transaction_time = Carbon::now();
        $transaction->user_id = Auth::user()->id;
        $transaction->has_change = $request->has_change_for;
        $transaction->status = 1;
        $transaction->save();

        $id = $transaction->id;

        #dd($id);
        $n = 0;
        $cart_ids = [];
    	foreach ($request->quantity as $key => $value) {
 			$cart = Cart::whereId($key)->first();
    		$transaction_menu = new OnlineTransactionMenu;
    		$transaction_menu->transaction_id = $id;
    		$transaction_menu->menu_id = $cart->menu_id;
    		$transaction_menu->quantity = $value;
    		$transaction_menu->price_id = $cart->price_id;
        	$transaction_menu->save();

        	$cart_ids[$n] = $key;
        	$n++;
    	}

    	Cart::whereIn('id', $cart_ids)->delete();
       
        $data = [
            'message' => 'Menu has been updated.',
            'id' => $id,
            'name' => $transaction->user_id
        ];

        return redirect()->route('user_success_order', $id)->with('message', '1');  
    }

    public function success_order($id)
    {

        $data = compact('id');
        return view('user.cart.success', $data);
    }
  
}
