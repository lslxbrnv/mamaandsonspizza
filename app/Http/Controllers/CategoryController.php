<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Response;
use \Carbon\Carbon;

use App\Category;

class CategoryController extends Controller
{
	public function index()
	{
		$categories = Category::get();

		$data = compact('categories'); 
		
		return view('admin.category.index', $data);
	}
    public function add_category_page()
    {
    	return view('admin.category.add');
    }

    public function verify_category(Request $request)
    {
        $request->validate([
            'category' => 'required|max:255|unique:menu_category,category,NULL,id,deleted_at,NULL'
        ]);

        if($request->id != null)
        {
            $category = Category::find($request->id);
            $category->category = $request->category;
            $category->updated_at = Carbon::now();
            $category->save();

            $data = [
                'message' => 'Category has been updated.',
                'name' => $request->category
            ];

            return redirect()->route('admin_edit_category_page', $request->id)->with('message', 'Success');
        }
        else{

            $category = new Category;
            $category->category = $request->category;
            $category->created_at = Carbon::now();
            $category->save();

            $data = [
                'message' => 'Category has been added.',
                'id' => $category->id,
                'name' => $request->category
            ];
            return redirect()->route('admin_add_category_page')->with('message', 'Success');
        }
    }

    public function edit_category_page($id)
    {

        $category = Category::whereId($id)->first()->category;
        #dd($category);

        $data = compact('category', 'id');

        return view('admin.category.edit', $data);
    }

    public function remove_category($id)
    {
        $category = Category::find($id);
        $category->delete();
        
        return redirect()->route('admin_category')->with('message', 'Success');
    }
}
