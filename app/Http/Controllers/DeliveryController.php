<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OnlineTransaction;
use App\OnlineTransactionMenu;
use \Carbon\Carbon;

class DeliveryController extends Controller
{
    public function index()
    {
    	$transactions = OnlineTransaction::where('status', 2)->get();

    	$data = compact('transactions');

    	return view('delivery.dashboard.index', $data);
    }

    public function selectOrder($id)
    {
    	$order = OnlineTransaction::find($id);
     #   dd($order);
   		$data = compact('order');
    	return view('delivery.dashboard.order', $data);
    }

    public function saveLocation(Request $request)
    {
        #dd($request);
        \App\DeliveryLocation::create($request->except('_token'));
    }
}
