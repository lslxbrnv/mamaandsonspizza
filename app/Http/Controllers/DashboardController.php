<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use App\OnlineTransaction;
use App\OnlineTransactionMenu;
use \Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard.index');
    }

    public function check_orders()
    {
        //
        $transactions = OnlineTransaction::where('status', '!=', 0)->get();

        $data = compact('transactions');

        return view('admin.dashboard.orders', $data);
    }

    public function check_order_details($id)
    {
        $transaction = OnlineTransaction::with('user')->whereId($id)->first();

        $orders = OnlineTransactionMenu::with('menu')->with('pricing')->where('transaction_id', $id)->get();

        $data = compact('transaction', 'orders');

        return view('admin.dashboard.orders-details', $data);
    }


    public function receive_order($id)
    {
       # dd($id);
        $update_transaction_status = Transaction::find($id);
        if($update_transaction_status->status == 1){
            $update_transaction_status->status = 2;
        }else if($update_transaction_status->status == 2)
        {
            $update_transaction_status->status = 3;

        }
        $update_transaction_status->updated_at = Carbon::now();
        $update_transaction_status->save();
        
        return redirect()->route('admin_check_orders')->with('message', 'Status set to received.');

    }

    public function create_account()
    {
        return view('admin.accounts.index');
    }

    public function add_account(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',
            'name' => 'required'
        ]);

        $check = User::where('username',  $request->username)->orWhere('email', $request->email)->count();
        #dd($check);
        if($check > 0)
        {
            return redirect()->route('admin_create_account')->with('message', 'Username or e-mail already exists.');
        }else{

            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
            $user->access = 3;
            $user->created_at = Carbon::now();
            $user->save();
        }
        
        return redirect()->route('admin_create_account')->with('message', 'User Created Successfully');

    }

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
