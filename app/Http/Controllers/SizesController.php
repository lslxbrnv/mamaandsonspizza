<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Response;
use \Carbon\Carbon;

use App\Sizes;
use App\Menu;

class SizesController extends Controller
{
    public function index()
    {
    	$sizes = Sizes::get();

    	$data = compact('sizes');

    	return view('admin.sizes.index', $data);
    }
    public function add_menu_sizes()
    {

    	return view('admin.sizes.add');
    }

    public function verify_added_size(Request $request)
    {
    	$this->validate($request, [
            'size' => 'required|unique:sizes,size,NULL,id,deleted_at,NULL|max:255'
        ]);

        $size = new Sizes;
        $size->size = $request->size;
        $size->created_at = Carbon::now();
        $size->save();

        $data = [
            'message' => 'Size has been added.',
            'id' => $size->id,
            'size' => $request->size
        ];

        return redirect()->route('add_menu_sizes')->with('message', 'Success');
    }

    public function remove_size($id)
    {
		$size = Sizes::find($id);
        $size->delete();

        return redirect()->route('menu_sizes')->with('message', 'Success');
    }
}
