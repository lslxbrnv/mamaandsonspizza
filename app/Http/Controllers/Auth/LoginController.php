<?php

namespace App\Http\Controllers\Auth;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;

use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
#  protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

      public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

     public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)
            ->stateless()
            ->setHttpClient(new \GuzzleHttp\Client(['verify' => false]))
            ->user();

        $authUser = $this->findOrCreateUser($user, $provider);

        Auth::login($authUser, true);

         return redirect()->route('user_dashboard');
    }

    public function findOrCreateUser($user, $provider)
    {
        #dd($user);
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'provider_id' => $user->id,
            'email'    => $user->email,
            'username'    => $user->email,
            'name'     => $user->name,
            'provider' => $provider,
            'access' => 3
        ]); 
    }

    public function username()
    {
        return "username";
    }


    protected function authenticated(Request $request, $user)
    {
        $status = Auth::user()->deleted_at;
        $access = Auth::user()->access;

        if($status != null) {
            Auth::logout();
            return redirect()->route('login')->with('login-message', 'Login is disabled!');
        }   

      
            if($access == 1) {
                return redirect()->route('admin_menu');
            }else if($access == 2) {
                return redirect()->route('user_dashboard');
            }else if($access === 3) {
                return redirect()->route('deliveryboy_dashboard');
            }else{
                dd('echo');
            }
    }
}
