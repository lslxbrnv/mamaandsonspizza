<?php

namespace App\Http\Controllers;
use Auth;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    //
     public function __construct()
    {

    }

    public function login_page()
    {
    	return view('login.index');
    }

    public function verify_login(Request $request)
    {
    	$username = trim($request->username);
        $password = trim($request->password);

        if (Auth::attempt(['username' => $username, 'password' => $password]))
        {
            $status = Auth::user()->deleted_at;
            $access = Auth::user()->access;

            if($status != null) {
                Auth::logout();
                return redirect()->route('login')->with('login-message', 'Login is disabled!');
            }   

            if($access == 1) {
                return redirect()->route('admin_menu');
            }else if($access == 2) {
                return redirect()->route('user_dashboard');
            }else if($access === 3) {
                return redirect()->route('deliveryboy_dashboard');
            }else{
                dd('echo');
            }
            
        }

        return redirect()->route('login')->with('login-message', 'Invalid Username/Password!');
    }	
}
