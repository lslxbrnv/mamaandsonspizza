<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Auth;
use Response;
use \Carbon\Carbon;

use App\User;

class UserDashboardController extends Controller
{
	public function index()
	{
		$user = User::find(Auth::user()->id);
		$data = compact('user');

    	return view('user.dashboard.index' ,$data);
	}

	public function update_info(Request $request)
	{
		$request->validate([
			'name' => 'required|max:255',
			'contact_number' => 'required|max:255',
			'address' => 'required|max:255',
			'email' => 'required|max:255'
		]);

		$user = User::find($request->user_id);
		$user->name = $request->name;
		$user->contact_number = $request->contact_number;
		$user->address = $request->address;
		$user->email = $request->email;
		$user->save();

		return redirect()->route('user_menu');
	}

}
