<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Response;
use \Carbon\Carbon;

use App\Category;
use App\Menu;
use App\Pricing;
use App\Cart;
use App\Sizes;

use App\OnlineTransaction;
use App\OnlineTransactionMenu;



class UserMenuController extends Controller
{
    public function index()
    {
    	$menus = Menu::with('category')->get();
       # dd($menus);
    	$data = compact('menus');

    	return view('user.menu.index', $data);
    }

    public function get_menu(Request $request)
    {
       # $pricing = DB::connection('myslq2')->select(...);
        $pricing = Pricing::with('sizes')
                          ->where('menu_id', $request->menuid)
                          ->get();
        foreach($pricing as $key => $value)
        {
           echo' <div class="control">
                <label class="radio">
                <input class="change-size" type="radio" name="menu_size" value="'.$value->id.'">
                <input class="hidden_price" type="hidden" name="hidden_price'.$value->id.'" value="'.$value->price.'">
                '.$value->sizes->size.' - <span class="size-price">'.$value->price.'</span>
                </label>
            </div>';
           # echo $value->sizes->size;
           # echo $value->price;
        }
        #return response()->json($pricing);
    }

    public function add_to_cart(Request $request)
	{
        $cart = new Cart;
        $cart->menu_id = $request->menu_id;
        $cart->price_id = $request->menu_size;
        $cart->user_id = Auth::user()->id;
        $cart->quantity = $request->quantity;
        $cart->created_at = Carbon::now();
        $cart->save();

        return redirect()->route('user_menu')->with('message', 'Success');
	}

    public function user_orders()
    {
        $transactions = OnlineTransaction::where('status','!=', 0)->where('user_id', Auth::user()->id)->get();

        $data = compact('transactions');
        
        return view('user.orders.index', $data);
    }

    public function user_order_details($id)
    {
        $order = OnlineTransaction::find($id);
        $order_menus = OnlineTransactionMenu::where('transaction_id' ,$id)->get();

        #return $order->online_transaction_menu;
        $data = compact('order', 'order_menus');
        return view('user.orders.details', $data);
    }

    public function user_order_location($id)
    {
        return \App\DeliveryLocation::select(['lat','long'])->where('order_id', $id)->latest()->first();
    }
}

