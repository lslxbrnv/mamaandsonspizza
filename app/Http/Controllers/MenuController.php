<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Auth;
use Response;
use \Carbon\Carbon;

use App\Category;
use App\Menu;
use App\Pricing;
use App\Sizes;

class MenuController extends Controller
{
    public function index()
	{
        $menus = Menu::with('category')->get();
      #  dd($menus);
        $data = compact('menus');
        
		return view('admin.menu.index', $data);
	}

    public function add_menu_page()
    {
        $categories = Category::get();

        $data = compact('categories');

    	return view('admin.menu.add', $data);
    }

    public function edit_menu_page($id)
    {
        $categories = Category::get();
        $menu = Menu::whereId($id)->first();
        $data = compact('id', 'categories', 'menu');

        return view('admin.menu.edit', $data);
    }

    public function verify_menu(Request $request)
    {
     #   dd($request->image);

        if($request->menu_id != null)
        {
            if(($request->menu_duplicate == $request->menu) && ($request->category_duplicate == $request->category))
            {
                if ($request->hasFile('image')) {
                    $this->validate($request, [
                        'details' => 'required',
                        'image' => 'required|image|mimes:jpeg|max:2048'
                    ]);
                    
                    $imageName = time().'.'.request()->image->getClientOriginalExtension();
                    request()->image->move(public_path('images'), $imageName);

                    $menu = Menu::find($request->menu_id);
                    $menu->details = $request->details;
                    $menu->image = $imageName;
                    $menu->updated_at =Carbon::now();
                    $menu->save();

                }else{
                     $this->validate($request, [
                        'details' => 'required'
                    ]);

                    $menu = Menu::find($request->menu_id);
                    $menu->details = $request->details;
                    $menu->updated_at =Carbon::now();
                    $menu->save();

                }
               


                $menu = Menu::find($request->menu_id);
                $menu->details = $request->details;
                $menu->save();

                $data = [
                    'message' => 'Category has been updated.',
                    'name' => $request->menu
                ];

            }else{

                if ($request->hasFile('image')) {
                    $this->validate($request, [
                        'category' => 'required',
                        'menu' => 'required|unique:menu,menu,NULL,id,category_id,'.$request->category.',deleted_at,NULL|max:255',
                        'details' => 'required',
                        'image' => 'required|image|mimes:jpeg|max:2048'
                    ]);
                    
                    $imageName = time().'.'.request()->image->getClientOriginalExtension();
                    request()->image->move(public_path('images'), $imageName);

                    $menu = Menu::find($request->menu_id);
                    $menu->category_id = $request->category;
                    $menu->menu = $request->menu;
                    $menu->updated_at = Carbon::now();
                    $menu->image = $imageName;
                    $menu->save();
                    
                }else{
                    $this->validate($request, [
                        'category' => 'required',
                        'menu' => 'required|unique:menu,menu,NULL,id,category_id,'.$request->category.',deleted_at,NULL|max:255',
                        'details' => 'required'
                    ]);

                    $menu = Menu::find($request->menu_id);
                    $menu->category_id = $request->category;
                    $menu->menu = $request->menu;
                    $menu->save();
                }
                
                
            }
            return redirect()->route('admin_edit_menu_page', $request->menu_id)->with('message', 'Success');
        }else{
            $this->validate($request, [
                'category' => 'required',
                'menu' => 'required|unique:menu,menu,NULL,id,category_id,'.$request->category.',deleted_at,NULL|max:255',
                'details' => 'required',
                'image' => 'required|image|mimes:jpeg|max:2048'
            ]);

            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

            $menu = new Menu;
            $menu->category_id = $request->category;
            $menu->menu = $request->menu;
            $menu->details = $request->details;
            $menu->created_at = Carbon::now();
            $menu->image = $imageName;
            $menu->save();

            $data = [
                'message' => 'Menu has been added.',
                'id' => $menu->id,
                'name' => $request->name
            ];
            return redirect()->route('admin_add_menu_page')->with('message', 'Success');

        }
       
    }

    public function remove_menu($id)
    {
        $pricing = Menu::find($id);
        $pricing->delete();

        return redirect()->route('admin_manage_sizes', $pid)->with('message', 'Success');
    }

    public function manage_sizes($id)
    {   
        $pricing = Pricing::with('sizes')->where('menu_id', $id)->get();

        $sizes = Sizes::get();

        $menu = Menu::find($id);

        $data = compact('id', 'pricing', 'sizes', 'menu');

        return view('admin.menu.sizes', $data);
    }

    public function add_price_size(Request $request)
    {
        $this->validate($request, [
            'size' => 'required',
            'price' => 'required|numeric',
            'menu_id' => 'unique:pricing,menu_id,NULL,id,size_id,'.$request->size.',deleted_at,NULL'
        ]);

        $pricing = new Pricing;
        $pricing->menu_id = $request->menu_id;
        $pricing->size_id = $request->size;
        $pricing->price = $request->price;
        $pricing->created_at = Carbon::now();
        $pricing->save();

        $data = [
            'message' => 'Menu has been updated.',
            'id' => $pricing->id,
            'name' => $request->menu_id
        ];

        return redirect()->route('admin_manage_sizes', $request->menu_id)->with('message', 'Success');        
    }

    public function remove_menu_price($id, $pid)
    {
        $pricing = Pricing::find($id);
        $pricing->delete();

        return redirect()->route('admin_manage_sizes', $pid)->with('message', 'Success');
    }
}
