<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    protected $connection = 'mysql2';
    
    use SoftDeletes;
    public $table = 'menu';
    public $fillable = ['category_id', 'menu', 'selling_price', 'user_id', 'created_at', 'image', 'details']; 
    public $timestamps = false;

    protected $dates = ['deleted_at'];

    public function category()
    {
    	return $this->belongsTo('App\Category')->withTrashed();
    }

    public function pricing()
    {
    	return $this->hasMany('App\Pricing');
    }

    public function cart()
    {
        return $this->hasMany('App\Cart');
    }

    public function transaction_menu()
    {
        return $this->belongsTo('App\TransactionMenu');
    }

}
