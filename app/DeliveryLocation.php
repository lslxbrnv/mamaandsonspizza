<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryLocation extends Model
{
    protected $connection = 'mysql2';
    public $table = 'delivery_location';
    public $timestamps = true;

    protected $fillable = [
        'order_id', 'lat', 'long'
    ];
}
