<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    public $table = 'menu_category';
    public $fillable = ['category',  'created_at', 'updated_at' ];  
    public $timestamps = false;

    protected $dates = ['deleted_at'];

    public function menu()
    {
    	return $this->hasMany('App\Menu')->withTrashed();
    }

}
