<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pricing extends Model
{
    use SoftDeletes;
    public $table = 'pricing';
    public $fillable = ['price', 'menu_id', 'size_id', 'created_at', 'updated_at' ];  
    public $timestamps = false;

    protected $connection = 'mysql2';
    
    protected $dates = ['deleted_at'];

    public function menu()
    {
    	return $this->belongsTo('App\Menu')->withTrashed();
    }
    
    public function sizes()
    {
    	return $this->belongsTo('App\Sizes', 'size_id', 'id')->withTrashed();
    }

    public function cart()
    {
        return $this->hasMany('App\Cart');
    }

    public function transaction_menu()
    {
        return $this->belongsTo('App\TransactionMenu');
    }
}
